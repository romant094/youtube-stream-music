import React from 'react'

export const Stream = ({
  src = 'https://www.youtube.com/embed/_daTfgc4u3k'
}) => {
  return (
    <iframe
      width='1222'
      height='687'
      src={src}
      title='YouTube video player'
      frameBorder='0'
      allow='accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture'
      allowFullScreen
    />
  )
}
